﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Analyticli.Core.Services;
using Analyticli.Core.ViewModels;

namespace Analyticli.Core
{
	public class App : MvxApplication
	{
		public App()
		{
			Mvx.RegisterSingleton<IMvxAppStart>(new MvxAppStart<ReportViewModel>());
			Mvx.RegisterSingleton<IFilterService>(new FilterService());
			Mvx.RegisterSingleton<IPBIReportService>(new PBIReportService());
		}
	}
}
