﻿
namespace Analyticli.Core
{
	public static class AppConstants
	{
		public const string ApiAddress = "https://analyticliweb.azurewebsites.net/api/";
		public const string CompanyFilterPath = "company/get";
		public const string DepartmentFilterPath = "department/GetChildren/{0}";
		public const string DateFilterPath = "date/GetChildren/{0}";
	}

	public class AuthenticationConfiguration
	{
		public const string ClientId = "8d2aa499-dd3a-41a2-bd04-367baa759e82";
		public const string ClientSecret = "Nrr2OQP0FxebQIksYHoPdV3Sjpeb57FJJj7wLpaGEos=";
		public const string TenantId = "b41b72d0-4e9f-4c26-8a69-f949f367c91d";
		public const string AADInstance = "https://login.windows.net/";
		public const string ReturnUrl = "http://analyticliios.azurewebsites.net/";
		public const string GraphResourceUri = "https://graph.windows.net";
	}
}
