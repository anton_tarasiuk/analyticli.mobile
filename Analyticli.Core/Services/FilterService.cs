﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Analyticli.Core.Services.Models;

namespace Analyticli.Core.Services
{
	public class FilterService : IFilterService
	{
		private const string CompanyFilterID = "company";
		private const string DepartmentFilterID = "department";
		private const string DateFilterID = "yeartodate";

		private HttpClient _client;

		private HttpClient Client
		{
			get
			{
				if (_client == null)
				{
					_client = new HttpClient();
					_client.BaseAddress = new Uri(AppConstants.ApiAddress);
					_client.DefaultRequestHeaders.Accept.Add(
						new MediaTypeWithQualityHeaderValue("application/json"));
				}
				return _client;
			}
		}

		private async Task<string> GetResponse(string filterId, string itemId)
		{
			string path;
			switch (filterId.ToLower().Replace(" ", ""))
			{
				case CompanyFilterID:
					path = AppConstants.CompanyFilterPath;
					break;
				case DepartmentFilterID:
					path = string.Format(AppConstants.DepartmentFilterPath, string.IsNullOrEmpty(itemId) ? "null" : itemId);
					break;
				case DateFilterID:
					path = string.Format(AppConstants.DateFilterPath, string.IsNullOrEmpty(itemId) ? "null" : itemId);
					break;
				default:
					path = string.Empty;
					break;
			}

			try
			{
				var response = Client.GetAsync(path).Result;
				return await response.Content.ReadAsStringAsync();
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}

		public async Task<List<ExtendedFilterData>> GetFilterData(string filterId, string itemId)
		{
			var response = await GetResponse(filterId, itemId);
			var res = JsonConvert.DeserializeObject<List<ExtendedFilterData>>(response).ToList();
			return res.Select(fd => new ExtendedFilterData(fd, filterId)).ToList(); ;
		}
	}
}
