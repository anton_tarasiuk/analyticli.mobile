﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Analyticli.Core.Services.Models;


namespace Analyticli.Core.Services
{
	public interface IFilterService
	{
		/// <summary>
		/// Get filter data.
		/// </summary>
		/// <param name="filterId"></param>
		/// <param name="itemId">itemId to get it's children </param>
		/// <returns></returns>
		Task<List<ExtendedFilterData>> GetFilterData(string filterId, string itemId = null);
	}
}
