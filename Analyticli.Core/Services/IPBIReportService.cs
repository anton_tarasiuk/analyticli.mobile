﻿namespace Analyticli.Core.Services
{
	public interface IPBIReportService
	{
		/// <summary>
		///  Get script function name to set filters data and refresh dashboard.
		/// </summary>
		/// <param name="companies"></param>
		/// <param name="departments"></param>
		/// <param name="dates"></param>
		/// <returns></returns>
		string SetFiltersScript(int[] companies, int[] departments, int[] dates);
		
		/// <summary>
		/// Get script function name to get pages.
		/// </summary>
		/// <returns></returns>
		string GetPagesScript();

		/// <summary>
		///  Get script function name to set page.
		/// </summary>
		/// <param name="page">Unique Name to define the page.</param>
		/// <returns></returns>
		string ChangePageScript(string page);
	}
}
