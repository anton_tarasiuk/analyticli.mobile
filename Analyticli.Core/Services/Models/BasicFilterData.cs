﻿
namespace Analyticli.Core.Services.Models
{
	public class BasicFilterData
	{
		private string _name;
		private string _id;
		private int _key;
		public string ID { get { return _id; } set { _id = value; } }

		public int Key { get { return _key; } set { _key = value; } }

		public string Name { get { return _name; } set { _name = value; } }
	}
}
