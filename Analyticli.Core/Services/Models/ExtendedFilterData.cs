﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Platform;
using Analyticli.Core.Services;

namespace Analyticli.Core.Services.Models
{
	public class ExtendedFilterData : BasicFilterData
	{
		private readonly string _filterId;
		private List<ExtendedFilterData> _children;
		private bool _checked;
		private string _parentId;
		private bool _hasChildren;
		private bool _opened;
		private int _levelIn;

		public string ParentID { get { return _parentId; } set { _parentId = value; } }
		public bool HasChildren { get { return _hasChildren; } set { _hasChildren = value; } }

		//custom properties
		public bool Checked { get { return _checked; } set { _checked = value; } }
		public bool Opened { get { return _opened; } set { _opened = value; } }
		public int LevelIn { get { return _levelIn; } set { _levelIn = value; } }

		public List<ExtendedFilterData> Children{
			get
			{
				if(_children!=null) return _children;
				if (!HasChildren) return null;

				_children = Mvx.GetSingleton<IFilterService>().GetFilterData(_filterId, ID).Result;
				return _children;
			}
		}

		public ExtendedFilterData() { }

		public ExtendedFilterData(ExtendedFilterData fd, string filterId)
		{
			_filterId = filterId;

			ID = fd.ID;
			Key = fd.Key;
			Name = fd.Name;

			ParentID = fd.ParentID;
			HasChildren = fd.HasChildren;
			Checked = fd.Checked;
			LevelIn = fd.LevelIn;
		}
		
	}
}
