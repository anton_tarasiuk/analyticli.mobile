﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyticli.Core.Services.Models
{
	public class ReportPage
	{
		public string Name { get; set; }
		public string DisplayName { get; set; }
	}
}
