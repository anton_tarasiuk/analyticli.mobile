﻿using System;
using System.IO;
using System.Reflection;
using Analyticli.Core.Services;

namespace Analyticli.Core.Services
{
	public class PBIReportService : IPBIReportService
	{
		public string SetFiltersScript(int[] companies, int[] departments, int[] dates)
		{
			string com = $"[{string.Join(",", companies)}]";
			string dep = $"[{string.Join(",", departments)}]";
			string dat = $"[{string.Join(",", dates)}]";

			return $"window.setFilters({com}, {dep}, {dat})";
		}

		public string GetPagesScript()
		{
			return "getPages()";
		}

		public string ChangePageScript(string page)
		{
			return $"changePage('{page}')";
		}

	}
}
