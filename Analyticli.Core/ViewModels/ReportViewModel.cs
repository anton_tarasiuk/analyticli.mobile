﻿using System;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using Analyticli.Core.Services;
using Analyticli.Core.Services.Models;

namespace Analyticli.Core.ViewModels
{
	public class ReportViewModel : MvxViewModel
	{
		private readonly IFilterService _filterService;
		//private readonly IPBIReportService _reportService;
		
		private List<ExtendedFilterData> _companyFilterData;
		private List<ExtendedFilterData> _departmentFilterData;
		private List<ExtendedFilterData> _yearToDateFilterData;

		public List<ExtendedFilterData> CompanyFilterData {
			get { return _companyFilterData; }
			set { _companyFilterData = value; }
		}

		public List<ExtendedFilterData> DepartmentFilterData {
			get { return _departmentFilterData; }
			set { _departmentFilterData = value; }
		}
		public List<ExtendedFilterData> YearToDateFilterData {
			get { return _yearToDateFilterData; }
			set { _yearToDateFilterData = value; }
		}

		public ReportViewModel(IFilterService filterService)
		{
			_filterService = filterService;

			InitializeFiltersData();
		}

		private async void InitializeFiltersData()
		{
			_companyFilterData = await _filterService.GetFilterData("company");
			_departmentFilterData = await _filterService.GetFilterData("department");
			_yearToDateFilterData = await _filterService.GetFilterData("yeartodate");
		}
	}
}
