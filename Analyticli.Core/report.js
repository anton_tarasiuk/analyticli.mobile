var reportService = (function() {
    var iframe;

    this.getReport = function (token, reportId, height, width) {
        iframe = document.getElementById('ifrTile');
        iframe.src = 'https://embedded.powerbi.com/appTokenReportEmbed?reportId=' + reportId;
        iframe.onload = function() {
            var msgJson =
            {
                action: "loadReport",
                accessToken: token,
                height: height,
                width: width
            };

            var msgTxt = JSON.stringify(msgJson);
            iframe.contentWindow.postMessage(msgTxt, "*");

        };
    };

})();

var onEmbedded = function ()
{
    window.webkit.messageHandlers.myapp.postMessage("123");
}

function onEmbedded() {
    window.webkit.messageHandlers.myapp.postMessage("456");
}
        //window.webkit.messageHandlers.myapp.postMessage(JSON.stringify(window.location));