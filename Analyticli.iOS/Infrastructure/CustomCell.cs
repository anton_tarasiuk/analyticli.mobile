using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;

namespace TestPBI.iOS.Infrastructure
{
	public class CustomCell : UITableViewCell
	{
		private UIImageView _arrowImageView;
		private UILabel _spaceControl;
		private UILabel _titleLabel;

		public CustomCell(string cellId): base(UITableViewCellStyle.Default, cellId)
		{
			_spaceControl = new UILabel();
			_arrowImageView = new UIImageView();
			_titleLabel = new UILabel();



		}
	}
}