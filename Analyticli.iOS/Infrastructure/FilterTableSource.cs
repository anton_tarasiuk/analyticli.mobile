﻿using System;
using System.Collections.Generic;
using Foundation;
using Analyticli.iOS.Views;
using UIKit;
using System.Linq;
using Analyticli.Core.Services.Models;

namespace Analyticli.iOS.Infrastructure
{
	public class FilterTableSource : UITableViewSource
	{
		protected string _cellId; 
		protected List<ExtendedFilterData> _tableItems;
		public bool _isMultiselectEnable;

		public event EventHandler RowSelectionChanged;

		public FilterTableSource(string cellId, List<ExtendedFilterData> items)
		{
			_cellId = cellId;
			_tableItems = items;
			_isMultiselectEnable = cellId.ToLower().Contains("department");
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell(_cellId);
			if (cell==null) cell = new UITableViewCell(UITableViewCellStyle.Default, _cellId);
			
			//set offset for child item
			var space = string.Empty;
			for (int j = 0; j < _tableItems[indexPath.Row].LevelIn; j++)
			{
				space += "> ";
			}
			
			cell.TextLabel.Text = space + _tableItems[indexPath.Row].Name;

			if (!_tableItems[indexPath.Row].HasChildren)
				cell.Accessory = _tableItems[indexPath.Row].Checked ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
			else
				cell.Accessory = UITableViewCellAccessory.None;
			
			return cell;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return _tableItems.Count;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{

			if (!_tableItems[indexPath.Row].HasChildren)
			{
				if (_tableItems[indexPath.Row].Checked)
				{
					tableView.CellAt(indexPath).Accessory = UITableViewCellAccessory.None;
					_tableItems[indexPath.Row].Checked = false;
				}
				else
				{
					if (!_isMultiselectEnable)
					{
						//TODO: UNCHECK all other cells
						tableView.BeginUpdates();

						var checkedItems = _tableItems.Where(x => x.Checked).ToList();

						foreach (var item in checkedItems)
						{
							item.Checked = false;
						}

						for (int i = 0; i < tableView.NumberOfRowsInSection(0); i++)
						{
							tableView.CellAt(NSIndexPath.FromItemSection(i, 0)).Accessory = UITableViewCellAccessory.None;
						}

						tableView.EndUpdates();
					}

					tableView.CellAt(indexPath).Accessory = UITableViewCellAccessory.Checkmark;
					_tableItems[indexPath.Row].Checked = true;

				}

				if (RowSelectionChanged != null)
					RowSelectionChanged(this, new FilterEventArgs() { SelectedItem = _tableItems[indexPath.Row] });
			}
			else
			{
				if (_tableItems[indexPath.Row].Opened)
				{
					//delete rows after this cell 
					CloseItemChildren(tableView, indexPath.Row);
				}
				else
				{
					//add rows after current cell

					//get children items data to add
					var childrenItems = _tableItems[indexPath.Row].Children;

					tableView.BeginUpdates();

					for (int i = 0; i < childrenItems.Count; i++)
					{
						childrenItems[i].LevelIn = _tableItems[indexPath.Row].LevelIn + 1;

						var ind = indexPath.Row + i + 1;
						_tableItems.Insert(ind, childrenItems[i]);
						tableView.InsertRows(new[] { NSIndexPath.Create(0, ind) }, UITableViewRowAnimation.Left);
					}

					tableView.EndUpdates();
					
					_tableItems[indexPath.Row].Opened = true;
				}

				tableView.ScrollEnabled = true;
			}
			
			tableView.DeselectRow(indexPath, true);
		}

		private void CloseItemChildren(UITableView tableView, int curIndex)
		{
			for (int i = 0; i < _tableItems[curIndex].Children.Count; i++)
			{
				if(_tableItems[curIndex + 1].Opened) CloseItemChildren(tableView, curIndex + 1);

				tableView.BeginUpdates();

				_tableItems.RemoveAt(curIndex + 1);
				tableView.DeleteRows(new[] { NSIndexPath.Create(0, curIndex + 1) }, UITableViewRowAnimation.Left);

				tableView.EndUpdates();
			}
			_tableItems[curIndex].Opened = false;
		}
	}

	public class FilterEventArgs : EventArgs
	{
		public ExtendedFilterData SelectedItem { get; set; }
	}
}
