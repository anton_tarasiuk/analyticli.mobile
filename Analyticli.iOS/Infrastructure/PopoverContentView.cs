﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Analyticli.Core.Services.Models;
using Analyticli.iOS.Resources;
using UIKit;

namespace Analyticli.iOS.Infrastructure
{
	public partial class PopoverContentView : UIViewController
	{
		private readonly string _contentId;
		public List<ExtendedFilterData> _items;
		private nfloat _maxContentHeight;
		private UITableView _companyFilterView;
		private UIView _mainView;
		private nfloat _contentHeight;
		private nfloat _popoverWidth;

		public event EventHandler ItemSelected;
		public event EventHandler ChildrenRequested;

		public PopoverContentView(string contentId, List<ExtendedFilterData> items, CGRect frame, out nfloat popoverWidth, out nfloat popoverHeight) : base("PopoverContentView", null)
		{
			_contentId = contentId;
			_items = items;

			_maxContentHeight = frame.Height - 350;

			var width = frame.Width/5;
			popoverWidth = width < 250 ? 250 : width;
			_popoverWidth = popoverWidth;

			var contentHeight = _items.Count * 44;
			_contentHeight = contentHeight > _maxContentHeight ? _maxContentHeight : contentHeight;
			popoverHeight = _contentHeight + 10;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			
			InitializeComponents();
		}

		void InitializeComponents()
		{
			_mainView = new UIView();
			_mainView.TranslatesAutoresizingMaskIntoConstraints = false;

			View.AddSubview(_mainView);
			View.AddConstraints(new[]
			{
				NSLayoutConstraint.Create(_mainView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, View, NSLayoutAttribute.Top, 1, 5),
				NSLayoutConstraint.Create(_mainView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, View, NSLayoutAttribute.Left,  1, 5),
				NSLayoutConstraint.Create(_mainView, NSLayoutAttribute.Right, NSLayoutRelation.Equal, View, NSLayoutAttribute.Right, 1, -5),
				NSLayoutConstraint.Create(_mainView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, View, NSLayoutAttribute.Bottom, 1, -5)
			});
			
			//table
			var tableSource = new FilterTableSource(_contentId, _items);
			tableSource.RowSelectionChanged += (sender, args) =>
			{ if (ItemSelected != null)
					ItemSelected(sender, args); };

			_companyFilterView = new UITableView(new CGRect(0, 0, _popoverWidth - 10, _contentHeight))
			{
				TranslatesAutoresizingMaskIntoConstraints = false,
				ScrollEnabled = _contentHeight >= _maxContentHeight,
				SeparatorColor = UIColor.LightGray,
				SeparatorInset = UIEdgeInsets.Zero,
				LayoutMargins = UIEdgeInsets.Zero,
				Source = tableSource
			};

			_mainView.AddSubview(_companyFilterView);
		}
	}
}

