
using UIKit;

namespace Analyticli.iOS.Resources
{
	public static class Styles
	{
		public static UIColor Primary_DarkBule = new UIColor(47.0f/255.0f, 65.0f/255.0f, 84.0f/255.0f, 1.0f);
		public static UIColor Primary_Green = new UIColor(91.0f / 255.0f, 194.0f / 255.0f, 168.0f / 255.0f, 1.0f);
		public static UIColor Secondary_Green = new UIColor(162.0f / 255.0f, 210.0f / 255.0f, 210.0f / 255.0f, 1.0f);
		public static UIColor Secondary_Red = new UIColor(221.0f / 255.0f, 92.0f / 255.0f, 71.0f / 255.0f, 1.0f);
		public static UIColor Secondary_Yellow = new UIColor(226.0f / 255.0f, 177.0f / 255.0f, 56.0f / 255.0f, 1.0f);
		public static UIColor Secondary_Blue = new UIColor(97.0f / 255.0f, 152.0f / 255.0f, 209.0f / 255.0f, 1.0f);

		public static UIColor PBI_BckgrGray = new UIColor(230.0f / 255.0f, 232.0f / 255.0f, 235.0f / 255.0f, 1.0f);

	}
}