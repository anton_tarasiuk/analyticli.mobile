using System;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Analyticli.Core;

namespace Analyticli.iOS.Services
{
	/// <summary>
	/// Provides Azure AD authentication methods.
	/// </summary>
	public class AuthenticationService
	{
		#region Private Members

		/// <summary>
		/// The synchronization object
		/// </summary>
		private static object syncObject = new object();

		/// <summary>
		/// The <see cref="AuthenticationService"/> service member
		/// </summary>
		private static AuthenticationService authenticationService = null;

		/// <summary>
		/// The <see cref="AuthenticationContext"/> member.
		/// </summary>
		private AuthenticationContext context = null;

		#endregion

		#region Constructors

		/// <summary>
		/// Prevents a default instance of the <see cref="AuthenticationService"/> class from being created.
		/// </summary>
		private AuthenticationService()
		{
			string authority = string.Concat(AuthenticationConfiguration.AADInstance, AuthenticationConfiguration.TenantId);

			this.context = new AuthenticationContext(authority);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the current instance of <see cref="AuthenticationService"/>.
		/// </summary>
		/// <value>
		/// The isntance of <see cref="AuthenticationService"/>.
		/// </value>
		public static AuthenticationService Current
		{
			get
			{
				if (authenticationService == null)
				{
					lock (syncObject)
					{
						if (authenticationService == null)
						{
							authenticationService = new AuthenticationService();
						}
					}
				}

				return authenticationService;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Aquires the token for the activity.
		/// </summary>
		/// <param name="activity">The activity.</param>
		/// <returns>The security token</returns>
		public async Task<AuthenticationResult> AquireToken(IPlatformParameters parameters)
		{
			Uri returnUri = new Uri(AuthenticationConfiguration.ReturnUrl);

			return await this.context.AcquireTokenAsync(AuthenticationConfiguration.GraphResourceUri, AuthenticationConfiguration.ClientId, returnUri, parameters);
		}

		#endregion
	}
}