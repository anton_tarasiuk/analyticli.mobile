using System;
using System.IO;
using Foundation;
using MvvmCross.iOS.Views;
using MvvmCross.Platform;
using Analyticli.Core.ViewModels;
using Analyticli.Core.Services;
using UIKit;
using WebKit;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Analyticli.iOS.Infrastructure;
using Analyticli.Core.Services.Models;
using Analyticli.iOS.Resources;
using Newtonsoft.Json;
using Analyticli.iOS.Services;

namespace Analyticli.iOS.Views
{
	public partial class ReportView : MvxViewController<ReportViewModel>, IWKScriptMessageHandler, IWKNavigationDelegate
	{
		private readonly string LocalHtmlUrl = Path.Combine(NSBundle.MainBundle.BundlePath, "Content/layout.html");

		#region user interface

		private UIView _mainView;
		private UIView _header;
		private UIView _filters;
		private UIButton _companyFilterButton;
		private UIButton _departmentFilterButton;
		private UIButton _yearToDateFilterButton;
		private UIButton _jobFilterButton;
		private UIButton _managerFilterButton;
		private UIButton _locationFilterButton;
		private UISegmentedControl _tab;
		private UIView _reportView;
		private WKWebView _reportWebView;

		#endregion

		#region data source

		private List<ExtendedFilterData> _companyFilterValues;
		private List<ExtendedFilterData> _departmentFilterValues;
		private List<ExtendedFilterData> _yearToDateFilterValues;
		private List<ExtendedFilterData> _jobFilterValues = new List<ExtendedFilterData>() { new ExtendedFilterData() };
		private List<ExtendedFilterData> _managerFilterValues = new List<ExtendedFilterData>() { new ExtendedFilterData() };
		private List<ExtendedFilterData> _locationFilterValues = new List<ExtendedFilterData>() { new ExtendedFilterData() };

		private List<ReportPage> _reportPages;
		private IPBIReportService _reportService;
		private UIView _tabView;
		private WKWebView _webView;
		private int _topSystemBarOffset;
		private float _tabsViewHeight;
		private float _headerHeight;

		#endregion

		private nfloat FiltersHeight
		{
			get { return IsPortrait ? 141 : 66; }
		}

		private CGRect[] FilterFrames
		{
			get
			{
				nfloat mrg, wdth;
				if (IsPortrait)
				{
					mrg = View.Bounds.Width / 16;
					wdth = mrg * 4;
					return new CGRect[]
					{
						new CGRect(mrg, 33, wdth, 30),
						new CGRect(mrg, 78, wdth, 30),
						new CGRect(2*mrg + wdth, 33, wdth, 30),
						new CGRect(2*mrg + wdth, 78, wdth, 30),
						new CGRect(3*mrg + 2*wdth, 33, wdth, 30),
						new CGRect(3*mrg + 2*wdth, 78, wdth, 30)
					};
				}
				else
				{
					mrg = View.Bounds.Width / 55;
					wdth = mrg * 8;
					return new CGRect[]
					{
						//new CGRect(mrg, 33, wdth, 30),
						//new CGRect(2*mrg + wdth, 33, wdth, 30),
						//new CGRect(3*mrg + 2*wdth, 33, wdth, 30),
						//new CGRect(4*mrg + 3*wdth, 33, wdth, 30),
						//new CGRect(5*mrg + 4*wdth, 33, wdth, 30),
						//new CGRect(6*mrg + 5*wdth, 33, wdth, 30),
						new CGRect(mrg, 18, wdth, 30),
						new CGRect(2*mrg + wdth, 18, wdth, 30),
						new CGRect(3*mrg + 2*wdth, 18, wdth, 30),
						new CGRect(4*mrg + 3*wdth, 18, wdth, 30),
						new CGRect(5*mrg + 4*wdth, 18, wdth, 30),
						new CGRect(6*mrg + 5*wdth, 18, wdth, 30),
					};
				}
			}
		}

		public bool IsPortrait
		{
			get
			{
				switch (InterfaceOrientation)
				{
					case UIInterfaceOrientation.LandscapeLeft:
					case UIInterfaceOrientation.LandscapeRight:
						return false;
					case UIInterfaceOrientation.Portrait:
					case UIInterfaceOrientation.PortraitUpsideDown:
					default:
						return true;
				}
			}
		}

		private CGRect InitReportViewFrame
		{
			get
			{
				var vertViewOffset = _topSystemBarOffset + _headerHeight + _tabsViewHeight + FiltersHeight;
				var viewHeight = _mainView.Bounds.Height - _topSystemBarOffset - _headerHeight - _tabsViewHeight - FiltersHeight;

				return new CGRect(0, vertViewOffset, _mainView.Bounds.Width, viewHeight);
			}
		}

		private CGRect InitReportWebViewFrame
		{
			get
			{
				var wvWidth = _mainView.Bounds.Width + 15;
				var wvHeight = wvWidth / (16/8);

				return new CGRect(-7.5, -15, wvWidth, wvHeight + 7.5);
			}
		}

		public ReportView()
		{

		}

		public ReportView(IntPtr handle) : base(handle)
		{

		}

		public override async void ViewDidLoad()
		{
			base.ViewDidLoad();

			IPlatformParameters parameters = new PlatformParameters(this);
			//var token = await AuthenticationService.Current.AquireToken(parameters);

			_companyFilterValues = ViewModel.CompanyFilterData;
			_departmentFilterValues = ViewModel.DepartmentFilterData;
			_yearToDateFilterValues = ViewModel.YearToDateFilterData;

			_reportService = Mvx.GetSingleton<IPBIReportService>();

			InitializeUIComponents();
		}

		private void InitializeUIComponents()
		{
			NavigationController.SetNavigationBarHidden(true, true);

			View.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			View.BackgroundColor = UIColor.White;

			_mainView = new UIView(View.Frame);

			//_mainView.TranslatesAutoresizingMaskIntoConstraints = false;

			_mainView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			_mainView.BackgroundColor = UIColor.White;
			View.AddSubview(_mainView);
			
			_topSystemBarOffset = 20;

			InitializeHeaderView();
			
			InitializeTabsView();
			
			InitializeFiltersView();

			InitializeReportView();
		}

		private void InitializeReportView()
		{
			_reportView = new UIView(InitReportViewFrame)
			{
				BackgroundColor = Styles.PBI_BckgrGray,
				TranslatesAutoresizingMaskIntoConstraints = false,
				ClipsToBounds = true,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth
			};

			var userController = new WKUserContentController();
			userController.AddScriptMessageHandler(this, "myapp");

			var config = new WKWebViewConfiguration
			{
				UserContentController = userController
			};

			_reportWebView = new WKWebView(InitReportWebViewFrame, config)
			{
				WeakNavigationDelegate = this,
				TranslatesAutoresizingMaskIntoConstraints = false,
				BackgroundColor = Styles.PBI_BckgrGray,
				AutoresizingMask = UIViewAutoresizing.FlexibleWidth
			};
			_reportWebView.LoadRequest(new NSUrlRequest(new NSUrl(LocalHtmlUrl, false)));

			_reportView.AddSubview(_reportWebView);
			_mainView.AddSubview(_reportView);

			InitReportScroll();
		}

		private void InitializeTabsView()
		{
			_tabsViewHeight = 59;
			_tab = new UISegmentedControl(new CGRect(20, 0, View.Bounds.Width - 40, 59));
			_tab.TranslatesAutoresizingMaskIntoConstraints = false;
			_tab.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			_tab.Layer.BorderWidth = 0;
			_tab.ApportionsSegmentWidthsByContent = true;

			_tab.SetTitleTextAttributes(new UITextAttributes() { Font = UIFont.FromName("ArialMT", 14f), TextColor = UIColor.LightGray }, UIControlState.Normal);
			_tab.SetTitleTextAttributes(new UITextAttributes() { Font = UIFont.FromName("ArialMT", 14f), TextColor = _tab.TintColor }, UIControlState.Selected);

			//TODO: show bottom border for selected segment
			//_tab.SetBackgroundImage(null, UIControlState.Normal, UIBarMetrics.Default);

			_tab.TintColor = UIColor.White;

			_tab.SelectedSegment = 0;
			_tab.ValueChanged += (sender, e) =>
			{
				ChangePage(_reportPages[(int)_tab.SelectedSegment].Name);
			};

			_tabView = new UIView(new CGRect(0, _topSystemBarOffset + _headerHeight, View.Bounds.Width, _tabsViewHeight));
			_tabView.BackgroundColor = UIColor.White;
			_tabView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			_tabView.AddSubview(_tab);

			_mainView.AddSubview(_tabView);
		}

		private void InitializeHeaderView()
		{
			_headerHeight = 100;
			_header = new UIView(new CGRect(0, _topSystemBarOffset, View.Bounds.Width, _headerHeight));
			_header.TranslatesAutoresizingMaskIntoConstraints = false;
			_header.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleLeftMargin;
			_header.BackgroundColor = Styles.Primary_DarkBule;

			var logo = new UIImageView(UIImage.FromBundle("logo.png"));
			logo.Frame = new CGRect(40, 22, 220, 57);
			_header.AddSubview(logo);

			var btn1 = new UIButton();
			btn1.SetImage(UIImage.FromFile("user.png"), UIControlState.Normal);
			btn1.Frame = new CGRect(_header.Bounds.Width - 80, 30, 48, 40);
			btn1.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;
			_header.AddSubview(btn1);

			var btn2 = new UIButton();
			btn2.SetImage(UIImage.FromFile("page-1.png"), UIControlState.Normal);
			btn2.Frame = new CGRect(_header.Bounds.Width - 110 - 14, 38, 14, 26);
			btn2.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;
			_header.AddSubview(btn2);

			var btn3 = new UIButton();
			btn3.SetImage(UIImage.FromFile("shape-copy-3.png"), UIControlState.Normal);
			btn3.Frame = new CGRect(_header.Bounds.Width - 149 - 24, 38, 24, 24);
			btn3.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;
			_header.AddSubview(btn3);

			var btn4 = new UIButton();
			btn4.SetImage(UIImage.FromFile("shape-copy-2.png"), UIControlState.Normal);
			btn4.Frame = new CGRect(_header.Bounds.Width - 198 - 24, 38, 24, 24);
			btn4.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;
			_header.AddSubview(btn4);

			var btn5 = new UIButton();//.FromType(UIButtonType.RoundedRect);
			btn5.SetImage(UIImage.FromFile("shape.png"), UIControlState.Normal);
			btn5.Frame = new CGRect(_header.Bounds.Width - 247 - 24, 38, 24, 24);
			btn5.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;
			_header.AddSubview(btn5);

			var btn6 = new UIButton();
			btn6.SetImage(UIImage.FromFile("shape-copy.png"), UIControlState.Normal);
			btn6.Frame = new CGRect(_header.Bounds.Width - 296 - 24, 38, 24, 24);
			btn6.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin;
			_header.AddSubview(btn6);

			_mainView.AddSubview(_header);
		}

		private void InitializeFiltersView()
		{
			_filters = new UIView(new CGRect(0, _topSystemBarOffset + _headerHeight + _tabsViewHeight, View.Bounds.Width, FiltersHeight));
			_filters.TranslatesAutoresizingMaskIntoConstraints = false;
			_filters.BackgroundColor = Styles.PBI_BckgrGray;

			_companyFilterButton = InitializeFilterComponent("Company", 0, _companyFilterValues);
			_departmentFilterButton = InitializeFilterComponent("Department", 1, _departmentFilterValues);
			_yearToDateFilterButton = InitializeFilterComponent("This Year", 2, _yearToDateFilterValues);
			_jobFilterButton = InitializeFilterComponent("Job Description", 3, _jobFilterValues);
			_managerFilterButton = InitializeFilterComponent("Manager", 4, _managerFilterValues);
			_locationFilterButton = InitializeFilterComponent("Location", 5, _locationFilterValues);

			_mainView.AddSubview(_filters);
			_mainView.BringSubviewToFront(_filters);
		}

		private UIButton InitializeFilterComponent(string title, int frameIndex, List<ExtendedFilterData> filterValues)
		{
			var filterButton = new UIButton()
			{
				TranslatesAutoresizingMaskIntoConstraints = false,
				AutoresizingMask = UIViewAutoresizing.FlexibleMargins,
				Font = UIFont.FromName("ArialMT", 14f),
				HorizontalAlignment = UIControlContentHorizontalAlignment.Left,
				BackgroundColor = UIColor.White
			};
			filterButton.SetTitleColor(UIColor.Gray, UIControlState.Normal);
			filterButton.SetTitle("  " + title, UIControlState.Normal);
			var imgView = new UIImageView(UIImage.FromFile("arrow.png"));
			filterButton.AddSubview(imgView);

			filterButton.Frame = FilterFrames[frameIndex];
			imgView.Frame = new CGRect(FilterFrames[frameIndex].Width - 23, 11, 12, 7.4);
			
			_filters.AddSubview(filterButton);

			nfloat popoverHeight;//calculated height depends on content
			nfloat popoverWidth; //calculated width depends on View.Frame
			var filterViewPopover = new PopoverContentView(title, filterValues, View.Bounds, out popoverWidth, out popoverHeight);

			filterButton.TouchUpInside += (sender, args) =>
			{
				var filterView = new UIPopoverController(filterViewPopover);
				filterView.PopoverContentSize = new CGSize(popoverWidth, popoverHeight);

				if (!filterView.PopoverVisible) { filterView.PresentFromRect(filterButton.Frame, _filters, UIPopoverArrowDirection.Up, true); }
				else { filterView.Dismiss(true); }
			};

			filterViewPopover.ItemSelected += (sender, e) =>
			{
				var args = e as FilterEventArgs;
				if (args != null)
				{
					if (args.SelectedItem != null) filterValues.First(x => x.ID == args.SelectedItem.ID).Checked = args.SelectedItem.Checked;
					var selectedFilterTitle = string.Join(", ", filterValues.Where(a => a.Checked).Select(b => b.Name));
					filterButton.SetTitle(string.IsNullOrEmpty(selectedFilterTitle) ? "  " + title : " " +  selectedFilterTitle, UIControlState.Normal);
					UpdateReport();
				}
			};

			return filterButton;
		}

		public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillAnimateRotation(toInterfaceOrientation, duration);

			RebuildFiltersBar();
			ResizeReport();
			InitReportScroll();
		}

		private void ResizeReport()
		{
			_reportView.Frame = InitReportViewFrame;
			_reportWebView.Frame = InitReportWebViewFrame;
		}

		private void InitReportScroll()
		{
			_reportWebView.ScrollView.ScrollEnabled = InitReportViewFrame.Height + 18 < InitReportWebViewFrame.Height;
			_reportWebView.ScrollView.ContentInset = IsPortrait ? new UIEdgeInsets(-10, 0, 0, 0) : new UIEdgeInsets(0, 0, 0, 0);
		}

		private void RebuildFiltersBar()
		{
			_filters.Frame = new CGRect(0, _topSystemBarOffset + _headerHeight + _tabsViewHeight, View.Bounds.Width, FiltersHeight);

			_companyFilterButton.Frame = FilterFrames[0];
			_companyFilterButton.Subviews.First().Frame = new CGRect(FilterFrames[0].Width - 23, 11, 12, 7.4);

			_departmentFilterButton.Frame = FilterFrames[1];
			_departmentFilterButton.Subviews.First().Frame = new CGRect(FilterFrames[1].Width - 23, 11, 12, 7.4);

			_yearToDateFilterButton.Frame = FilterFrames[2];
			_yearToDateFilterButton.Subviews.First().Frame = new CGRect(FilterFrames[2].Width - 23, 11, 12, 7.4);

			_jobFilterButton.Frame = FilterFrames[3];
			_jobFilterButton.Subviews.First().Frame = new CGRect(FilterFrames[3].Width - 23, 11, 12, 7.4);

			_managerFilterButton.Frame = FilterFrames[4];
			_managerFilterButton.Subviews.First().Frame = new CGRect(FilterFrames[4].Width - 23, 11, 12, 7.4);

			_locationFilterButton.Frame = FilterFrames[5];
			_locationFilterButton.Subviews.First().Frame = new CGRect(FilterFrames[5].Width - 23, 11, 12, 7.4);
		}

		#region script methods

		private void UpdateReport()
		{
			var companies = _companyFilterValues.Where(a => a.Checked).Select(b => b.Key).ToArray();
			var departments = _departmentFilterValues.Where(a => a.Checked).Select(b => b.Key).ToArray();
			var dates = _yearToDateFilterValues.Where(a => a.Checked).Select(b => b.Key).ToArray();


			_reportWebView.EvaluateJavaScript(_reportService.SetFiltersScript(companies, departments, dates), (result, error) =>
			{
				if (error != null)
				{
					Console.WriteLine(error);
					var msg = error.UserInfo.DescriptionInStringsFileFormat;
				}
				else
				{
					var res = result;
				}
			});
		}

		private void GetPages()
		{
			_reportWebView.EvaluateJavaScript(_reportService.GetPagesScript(), (result, error) =>
			{
				if (error != null)
				{
					Console.WriteLine(error);
					var msg = error.UserInfo.DescriptionInStringsFileFormat;
				}
				else
				{
					_reportPages = JsonConvert.DeserializeObject<List<ReportPage>>(result.ToString()).ToList();
					PopulateTabView(_reportPages);
				}
			});
		}

		private void PopulateTabView(List<ReportPage> pages)
		{
			for (int i = 0; i < pages.Count; i++)
			{
				_tab.InsertSegment(pages[i].DisplayName, i, false);
				_tab.SetWidth(180, i);
			}
			_tab.SelectedSegment = 0;

		}

		private void ChangePage(string pageName)
		{
			_reportWebView.EvaluateJavaScript(_reportService.ChangePageScript(pageName), (result, error) =>
			{
				if (error != null)
				{
					Console.WriteLine(error);
					var msg = error.UserInfo.DescriptionInStringsFileFormat;
				}
				else
				{
					var res = result;
				}
			});
		}

		#endregion

		[Export("webView:didFinishNavigation:")]
		public void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
		{
		}

		public void DidReceiveScriptMessage(WKUserContentController userContentController, WKScriptMessage message)
		{
			var msg = message.Body?.ToString();
			if (msg != null)
			{
				if (msg.Contains("Report loaded.")) GetPages();
			}

			System.Diagnostics.Debug.WriteLine(msg);
		}
	}
}